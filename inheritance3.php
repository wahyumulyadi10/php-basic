<?php

abstract class Model{
    public $type;
    public function __construct($type){
        $this->type =$type;
    }
    abstract public function getType();
}
class Child extends Model{
    public function getType(){
        return $this->type;
    }
}

$model = new Child("Test Data");
echo $model->getType();
$abs = new Model("Testing 132");
// echo $abs->getType();