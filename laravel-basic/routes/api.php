<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LatihanController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('print-data', [LatihanController::class,'getData']);
Route::post('store',[LatihanController::class,'post']);
Route::post('show-data',[LatihanController::class,'shows']);
Route::post('show-data/{name}',[LatihanController::class,'getPerson']);
Route::post('login', [AuthController::class,'login']);
Route::post('register', [AuthController::class,'register']);
