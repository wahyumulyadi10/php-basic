<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Person;

class LatihanController extends Controller
{
    public function getData(){
        return "Hellow orang-orang.";
    }

    public function post(Request $request){
        $name = $request->input('name');
        $age = $request->input('age');

        $person = Person::create([
            "name" => $name,
            "age"  => $age
        ]
    );
    return response()->json(["status"=>"saved"],200);
    }

    public function shows(){
        return response()->json(Person::all(),200);
    }
    public function getPerson($name){
        $person = Person::where("name",$name);
        return response()->json($person->get(),200);
    }
}
