<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //
    public function register(Request $request){
        $data = $request->validate(
            [
            'email' => ['required', 'email'],
            'password' => ['required'],
            'name' => ['required']
            ]
            );
            $user = User::create([
                'name'=>$data['name'],
                'email'=>$data['email'],
                'password'=>Hash::make($data['password'])
            ]);

        if($user){
            return response()->json(["register"=>"success"],200);
        }
        return response()->json(["register"=>"failed"],200);
    }
    public function login(Request $request){
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return response()->json(["login"=>"success"],200);
        }

        return response()->json(["login"=>"failed"],200);
    }
}
