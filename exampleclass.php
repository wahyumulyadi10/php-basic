<?php

class Person
{

    private $name;
    public $age = 28;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
    public function setAge($age)
    {
        $this->age = $age;
    }

    public function getAge()
    {
        return $this->age;
    }
}
$person = new Person("Mulyadi");
// echo $person->name;
echo $person->age;
echo $person->getname();
echo $person->setName("Wahyu");
echo $person->getname();

