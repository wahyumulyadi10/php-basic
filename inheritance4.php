<?php

interface Blueprint{
    public function setDesign($name);
    public function getDesign();
}

class Rumah implements Blueprint{
    public $type;
    public $name;
    public function __construct($type)
    {
        $this->type = $type;
    }
    function setDesign($name)
    {
        $this->name = $name;
    }
    function getDesign()
    {
        return [$this->name => $this->type];
    }
}
$rumah = new Rumah("2 Lantai");
$rumah->setDesign("Kontrakan 3 Petak");
foreach ($rumah->getDesign() as $name => $type) {
    # code...
    echo $name. " Tipe ". $type;
}
// echo $rumah->getDesign();